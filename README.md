Finger Touch

An android application which allows a user to draw functionality. The basic functionality of the application will allow the user to select different types of shapes to draw with, and will allow them to change colours using the pallette selectors on the left side of the application.


Palette selection tools – allow the user to set the colour of the drawing object. The
number of palette options should be appropriate and should at minimum include Red,
Yellow , Green, Blue and Violet.
2. Shape tool – allow the user to set the shape of the drawing object. To start with allow
the user to select the triangle, square and circle drawing shapes.
3. Reset button – implement code to wipe / clear an existing drawing
4. Exit button – implement code to close the program
5. Touch screen drawing – implement code to allow the user to draw on screen using
their fingers, selecting the appropriate drawing shape, and colour at any time.

•	User can select a pen and draw a free form-line in the drawing area using finger touch functionality.
•	Implemented a save Button to save the drawing to the Gallery.
•	Change the layout of the application. Color palette is at the bottom and use Image Button to select from different shapes.
![Android.png](https://bitbucket.org/repo/ox8Rr6/images/4087936339-Android.png)